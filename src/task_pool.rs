// Custom task pool that prioritises dequeuing of tasks with lower height

use std::collections::{BinaryHeap, VecDeque};
use std::cmp::Reverse;

use crate::task::Task;
use crate::task_generator::TaskGenerator;

pub struct TaskPool {
    task_queue: Vec<VecDeque<TaskGenerator>>,
    task_priority: BinaryHeap<Reverse<usize>>,
    max_height: usize,
    max_children: usize,
    pub len: u64, // number of tasks remaining
    maximum_concurrent_branching_tasks: usize, // for assertion
}

impl TaskPool {
    pub fn new(max_height: usize, max_children: usize, maximum_concurrent_branching_tasks: usize) -> Self {
        let mut task_priority = BinaryHeap::new();
        task_priority.push(Reverse(max_height));
        Self {
            task_queue: vec![VecDeque::new(); max_height + 1],
            task_priority,
            max_height,
            max_children,
            len: 0,
            maximum_concurrent_branching_tasks,
        }
    }
    
    pub fn push(&mut self, task_generator: TaskGenerator) {
        if self.task_priority.is_empty()|| self.task_queue[task_generator.height].is_empty() {
            self.task_priority.push(Reverse(task_generator.height));
        } else {
            let task_pool_top = self.task_priority.peek().unwrap();
            if task_pool_top.0 > task_generator.height {
                self.task_priority.push(Reverse(task_generator.height));
            }
        }
        self.len += task_generator.len() as u64;
        self.task_queue[task_generator.height].push_back(task_generator);

        // Breadth-first approach should not exceed this limit + 64 (size of initial task queue)
        let max_tasks = self.maximum_concurrent_branching_tasks * self.max_height * self.max_children + 64;
        assert!(<u64 as TryInto<usize>>::try_into(self.len).unwrap() <= max_tasks);
    }

    pub fn pop(&mut self) -> Result<Task, &'static str> {
        if let Some(task_pool_priority) = self.task_priority.peek() {
            if let Some(task_generator) = self.task_queue[task_pool_priority.0].front_mut() {
                let task = task_generator.next().unwrap();
                self.len -= 1;
                if task_generator.len() == 0 {
                    self.task_queue[task_pool_priority.0].pop_front();
                    if self.task_queue[task_pool_priority.0].is_empty() {
                        self.task_priority.pop();
                    }
                }
                return Ok(task);
            }
        }
        Err("Task pool is empty")
    }
}