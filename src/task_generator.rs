use rand::{Rng, SeedableRng};
use crate::task::{Task, TYPE_ARRAY};

#[derive(Clone)]
pub struct TaskGenerator {
    rng: rand_chacha::ChaCha20Rng,
    pub height: usize,
    max_children: usize,
    generated_tasks: usize,
    num_tasks: usize,
}

impl TaskGenerator {
    pub fn new(seed: u64, height: usize, max_children: usize, max_num: usize) -> Option<Self> {
        let mut rng = rand_chacha::ChaCha20Rng::seed_from_u64(seed);
        let num_tasks = rng.gen_range(0..=max_num);
        if num_tasks == 0 {
            return None;
        }
        Some(Self {
            rng,
            height,
            max_children,
            generated_tasks: 0,
            num_tasks
        })
    }

    pub fn generate_initial(seed: u64, starting_height: usize, max_children: usize) -> Option<TaskGenerator> {
        Self::new(seed, starting_height, max_children, 64)
    }

    pub fn len(&self) -> usize {
        self.num_tasks - self.generated_tasks
    }
}

impl Iterator for TaskGenerator {
    type Item = Task;

    fn next(&mut self) -> Option<Self::Item> {
        if self.generated_tasks < self.num_tasks {
            self.generated_tasks += 1;
            Some(Task {
                typ: TYPE_ARRAY[self.rng.gen_range(0..TYPE_ARRAY.len())],
                seed: self.rng.gen(),
                height: self.height,
                max_children: self.max_children,
            })
        } else {
            None
        }
    }
}

