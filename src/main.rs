use std::{
    collections::{HashMap, VecDeque},
    time::Instant, mem::size_of,
};
use tokio::task::{JoinHandle, spawn};
use task::{Task, TaskType};
use task_pool::TaskPool;
use task_generator::TaskGenerator;

mod task_pool;
mod task_generator;

const MAX_RAM: usize = 1 * 1024 * 1024; // 1 GB is a safe value

#[tokio::main]
async fn main() {
    let (seed, starting_height, max_children) = get_args();

    eprintln!(
        "Using seed {}, starting height {}, max. children {}",
        seed, starting_height, max_children
    );

    // Number of concurrent tasks to spawn per cycle is limited by the amount of RAM available
    let ram_per_concurrent_task = size_of::<Task>() + size_of::<(u64, TaskGenerator)>()
            + size_of::<JoinHandle<(u64, Option<TaskGenerator>)>>();
    let task_pool_ram = size_of::<VecDeque<TaskGenerator>>() * (starting_height + 1) + size_of::<Vec<VecDeque<TaskGenerator>>>();
    let handle_vec_ram = size_of::<Vec<JoinHandle<(u64, Option<TaskGenerator>)>>>(); // + number of concurrent tasks
    let maximum_concurrent_tasks = (MAX_RAM - task_pool_ram - handle_vec_ram) / ram_per_concurrent_task;
    eprintln!("Maximum concurrent branching tasks set to {}", maximum_concurrent_tasks);

    let mut output: u64 = 0;
    let mut task_pool = TaskPool::new(starting_height, max_children, maximum_concurrent_tasks);
    // let mut leaf_counter = 0;
    let mut count_map = HashMap::new();
    let initial_task_generator = TaskGenerator::generate_initial(seed, starting_height, max_children).unwrap();
    task_pool.push(initial_task_generator);

    let start = Instant::now();
    while task_pool.len > 0 {
        let mut handles = Vec::<JoinHandle<(u64, Option<TaskGenerator>)>>::new();
        for _ in 0..maximum_concurrent_tasks {
            if let Ok(next) = task_pool.pop() {
                *count_map.entry(next.typ).or_insert(0usize) += 1;
                handles.push(spawn(async move {
                    next.execute()
                }));
            } else {
                break;
            }
        }

        for handle in handles {
            let (result, new_tasks) = handle.await.unwrap();
            output ^= result;
            if let Some(new_task_generator) = new_tasks {
                task_pool.push(new_task_generator);
            }
            
        }
    }
    
    let end = Instant::now();

    eprintln!("Completed in {} s", (end - start).as_secs_f64());

    println!(
        "{},{},{},{}",
        output,
        count_map.get(&TaskType::Hash).unwrap_or(&0),
        count_map.get(&TaskType::Derive).unwrap_or(&0),
        count_map.get(&TaskType::Random).unwrap_or(&0)
    );
}

// There should be no need to modify anything below

fn get_args() -> (u64, usize, usize) {
    let mut args = std::env::args().skip(1);
    (
        args.next()
            .map(|a| a.parse().expect("invalid u64 for seed"))
            .unwrap_or_else(|| rand::Rng::gen(&mut rand::thread_rng())),
        args.next()
            .map(|a| a.parse().expect("invalid usize for starting_height"))
            .unwrap_or(5),
        args.next()
            .map(|a| a.parse().expect("invalid u64 for seed"))
            .unwrap_or(5),
    )
}

mod task;
